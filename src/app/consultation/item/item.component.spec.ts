import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemComponent } from './item.component';

import { DebugElement, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatRadioModule,
  MatSlideToggleModule
} from '@angular/material';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatSlideToggleModule,
        MatRadioModule
      ],
      declarations: [ItemComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;

    // note: 'ng test -sm=false' (-sm = --sourcemaps) to see the real bug (Input value missing)
    component.option = {
      id: 1,
      name: "blood Pressure",
      show: true
    };

    fixture.detectChanges();
  });

  it('should create without error from the input value', () => {
    expect(component).toBeTruthy();
  });

  it('should create test the item name', () => {
    expect(fixture.nativeElement.querySelector('header span').innerText).toEqual(component.option.name);
  });

});



describe('ComponentUnderTestComponent', () => {
  let mockParentComponent: MockParentComponent;
  let mockParentFixture: ComponentFixture<MockParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatSlideToggleModule,
        MatRadioModule
      ],
      declarations: [ ItemComponent, MockParentComponent
      ],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    mockParentFixture = TestBed.createComponent(MockParentComponent);
    mockParentComponent = mockParentFixture.componentInstance;
    mockParentFixture.detectChanges();
  });

  it('should create mock parent component', () => {
    expect(mockParentComponent).toBeTruthy();
  });

  it('should test name value', () => {
    expect(mockParentFixture.nativeElement.querySelector('header span').innerText).toEqual('blood Pressure');
  });

  @Component({
    selector: `host-component`,
    template: `<app-item [option]="option"></app-item>`
  })
  class MockParentComponent {
    option: Object = 
      { id: 1,
        name: "blood Pressure",
        show: true
      }
    ;
  }
});
