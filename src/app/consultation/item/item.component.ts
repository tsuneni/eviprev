import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent implements OnInit {

  @Input() option: any = null;

  consultationItemForm: FormGroup;
  patientConsultation: {};
  storageId: string = 'patient_consultation_bp';

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {

    // TODO create a type for the patient consultation data
    // default values
    this.patientConsultation = {
      alert: false,
      dbp: '',
      sbp: '',
      diabetes: false,
      comment: ''
    };

    if (localStorage[this.storageId]) {
      console.log(localStorage[this.storageId]);
      this.patientConsultation = JSON.parse(localStorage[this.storageId]);
    }

    this.createForm();
  }

  createForm() {
    this.consultationItemForm = this.formBuilder.group({
      'alert': [{value: this.patientConsultation['alert'], disabled: false}],
      'dbp': [this.patientConsultation['dbp'], [
        Validators.required,
        Validators.minLength(2)
      ]],
      'sbp': [this.patientConsultation['sbp'], [
        Validators.required,
        Validators.minLength(2)
      ]],
      'diabetes': this.patientConsultation['diabetes']? '1' : '2',
      'comment': this.patientConsultation['comment'],
    });
  }

  onSubmit() {
    let stringToStore = '';

    // translating radio button values
    this.consultationItemForm.value.diabetes =
        this.consultationItemForm.value.diabetes === '1' ? true : false;

    // the object must be stored as a string
    stringToStore = JSON.stringify(this.consultationItemForm.value);

    // storage
    localStorage.setItem(this.storageId, stringToStore);

    // debug confirmation
    console.log(localStorage[this.storageId]);
  }

  toggleItem() {
    this.option.show = !this.option.show;
  }
}
