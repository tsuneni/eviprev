import { Component, OnInit } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.sass']
})
export class CoreComponent implements OnInit {
  
  opened = true;
  expand = true;
  isMobile = false;

  constructor(public media:ObservableMedia ) { }

  ngOnInit() {
    if (this.media.isActive('xs')) {
      this.loadMobileContent();
    }
  }

  loadMobileContent() { 
    this.isMobile = true;
    this.opened = false;
    this.expand = false;
  }

  sidenavToggle() {
    if (this.isMobile) {
      let sidenavMob = document.getElementById('sidenav-mob');
      sidenavMob.style.display = 'block';
    }
    this.opened = !this.opened;
    this.expand = !this.expand;
  }
}
