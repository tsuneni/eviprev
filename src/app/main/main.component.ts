import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  itemOptions: Object = [
    { id: 1,
      name: "Blood Pressure",
      show: true
    },
    { id: 2,
      name: "Tobacco",
      show: false
    },
    { id: 3,
      name: "Movement motion",
      show: false
    },
    { id: 4,
      name: "Colon cancer",
      show: false
    },
    { id: 5,
      name: "Depression",
      show: false
    },
    { id: 6,
      name: "Cardiovasculaire disease",
      show: false
    },
    { id: 7,
      name: "Diabetes",
      show: false
    },
    { id: 8,
      name: "Aortic aneurysm",
      show: false
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
