import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let appTitle: string = 'eviprev'; // TODO should be centralized

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    component.title = appTitle;
    fixture.detectChanges();
  });

  it('should test the app title', () => {
    expect(fixture.nativeElement.querySelector('footer > span').innerText).toEqual(appTitle);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
