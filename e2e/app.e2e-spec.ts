import { AppPage } from './app.po';
import {  
  browser, element, by
}
from 'protractor';  

// TODO read http://www.protractortest.org/#/style-guide

describe('eviprev App', () => {
  let page: AppPage;
  const appTitle = 'eviprev'; // TODO centralize some test data

  beforeAll(() => {
    // resetting local data to test in known environment
    //browser.executeScript('window.localStorage.clear();');
    // TODO check Storage is disabled inside 'data:' URLs.
  });

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();

    expect(page.getParagraphText()).toEqual(appTitle);
  });

  it('should toggle the alert switch', () => {
    let alertToggle = element(by.css('form .alertToggle'));  
    alertToggle.click();  
    
    expect(alertToggle.getAttribute('class')).toContain('mat-checked');
  });

  it('should toggle the diabetes switch', () => {
    let yesDiabetesSelector = element.all(by.css('form mat-radio-button')).get(0);
    yesDiabetesSelector.click();  
    
    expect(yesDiabetesSelector.getAttribute('class')).toContain('mat-radio-checked');
  });

  it('should write 123 in DBP field', () => {
    let dbpInput = element(by.css('input[formControlName=dbp]'));
    dbpInput.sendKeys('123');
    
    expect(dbpInput.getAttribute('value')).toEqual('123');
  });

  it('should write abc in sbp field', () => {
    let sbpInput = element(by.css('input[formControlName=sbp]'));
    sbpInput.sendKeys('abc');
    
    expect(sbpInput.getAttribute('value')).toEqual('abc');
  });

  it('should save, reload page and check the dbp field', () => {
    let saveButton = element(by.css('.save-button'));
    let dbpInput = element(by.css('input[formControlName=dbp]'));

    saveButton.click();
    page.navigateTo();
    
    expect(dbpInput.getAttribute('value')).toEqual('123');
  });
});
